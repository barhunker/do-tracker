import math
from robobrowser import RoboBrowser
import datetime, os

HOMELAT = 33.771678
HOMELON = -118.192446
OUTPUTDIR = 'output'
HOWMANYDAYS = 3
baseurl = "https://dola.com/"

def main():
        if not os.path.isdir(OUTPUTDIR):
                os.mkdir(OUTPUTDIR)
        
        for x in range(HOWMANYDAYS):
                date = datetime.datetime.today() + datetime.timedelta(days=x)
                #date = date.strftime('%Y/%m/%d')
                createHTML(date)
        
def createHTML(date = ''):
        if date:
                targetpage = baseurl + "events/" + date.strftime('%Y/%m/%d')#2018/09/22" + date
        else:
                targetpage = baseurl

        filename = date.strftime('%Y-%m-%d') + ".html"
        
        #open robobrowser session and navigate to the target page
        browser = RoboBrowser(allow_redirects=False ,user_agent='Mozilla/5.0', history=True, parser='html.parser')
        browser.open(targetpage)
        listingsPage = True
        listingCount = 0

        h = open(OUTPUTDIR + '/' +filename,'w',encoding='utf-8')

        h.write('<script src="../sorttable.js"></script>\n')
        h.write('<table class="sortable" style="width:100%" border=1>\n')
        h.write('<tr>\n')
        h.write('<th>Time</th>\n')
        h.write('<th>Name</th>\n')
        h.write('<th>Category</th>\n')
        h.write('<th>Venue</th>\n')
        h.write('<th>Map</th>\n')
        h.write('<th>Distance (Miles)</th>\n')
        h.write('</tr>\n')
        
        nextPage = browser.get_link("Next Page")
        while nextPage:
                nextPage = browser.get_link("Next Page")
                #parse each valid page

                found = browser.find_all("div", {"class" : "event-card"})

                for item in found:
                        #name time date category link locname lat lon loclink
                        
                        listingCount += 1
                        
                        datecombo = item.find("meta", {"itemprop" : "startDate"}).get("datetime")
                        date = datecombo.split("T")[0]
                        time = datecombo.split("T")[1].split("-")[0]
                        name = item.find("span", {"itemprop":"name"}).get_text()
                        for line in str(item).split('\n'):
                                if "ds-listing event-card" in line:
                                        category = line.split("category-")[1].split('"')[0]
                        eventlink = item.find('a', {"class":"ds-listing-event-title"}).get('href')
                        venuelink = item.find_all('a')[2].get('href')
                        venuename = item.find_all('a')[2].get_text()
                        mapslink = item.find_all('a')[1].get('href')
                        try:
                                lat = item.find('meta', {"itemprop":"latitude"}).get("content")
                                lon = item.find('meta', {"itemprop":"longitude"}).get("content")
                                
                        except:
                                lat = ''
                                lon = ''

                        #print(eventlink)

                        h.write('<tr>\n')
                        h.write('<th>' + str(date) + "<br>" + str(time) + '</th>\n')
                        h.write('<th><a href="'+ baseurl + eventlink +'">' + str(name) + '</a></th>\n')
                        h.write('<th>' + str(category) + '</th>\n')
                        h.write('<th><a href="'+ baseurl + venuelink +'">' + str(venuename) + '</a></th>\n')
                        h.write('<th><a href="'+ mapslink +'">' + str("map") + '</a></th>\n')

                        if lat:
                                distMiles = distance(lat,lon,HOMELAT,HOMELON)
                                h.write('<th>' + str("{:.2f}".format(float(distMiles))) + '</th>\n')
                        else:
                                h.write('<th></th>\n')
                                
                        
                        h.write('</tr>\n')

                        #csvPrint(f,datecombo,name,category,eventlink,venuename,venuelink,mapslink,lat,lon)

                
                #end parse
                if nextPage:
                        browser.follow_link(nextPage)
        print(listingCount)

        h.write('</table>')

        h.close()

def distanceMiles(lat1,lon1,lat2,lon2):
        R = 6373.0

        lat1 = float(lat1)
        lon1 = float(lon1)
        lat2 = float(lat2)
        lon2 = float(lon2)

        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = (sin(dlat/2))**2 + cos(lat1) * cos(lat2) * (sin(dlon/2))**2
        c = 2 * atan2(sqrt(a), sqrt(1-a))
        distancekm = R * c
        distancemiles = 0.621371 * distancekm

        return distancemiles

def distance(lat1,lon1,lat2,lon2):
        lat1 = float(lat1)
        lon1 = float(lon1)
        lat2 = float(lat2)
        lon2 = float(lon2)
        radius = 3959 # miles

        dlat = math.radians(lat2-lat1)
        dlon = math.radians(lon2-lon1)
        a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = radius * c

        return d

def csvPrint( f, *args):
        delim = "|^|"
        line = ''
        for item in args:
                line+=(str(item) + delim)
        f.write(line[:-(len(delim))]+"\n")


#useless shit below here
def cleanFilename(string):
        valid_chars = "-_.() QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890"
        return ''.join(c for c in string if c in valid_chars)

def cleanDescription(string):
        return string.replace('\\n', '\n').replace('\\r', '\r').replace('\/', '/')

if __name__ == '__main__':
        main()
